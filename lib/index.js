'use strict'

// The name of the package in order to give the Antora logger a useful name
const { name: packageName } = require('../package.json')
const fs = require('fs');
const { exec } = require('child_process');

var logger

module.exports.register = function ({ config }) {
    logger = this.require('@antora/logger').get(packageName)
    logger.info("registered")

    const OUTPUT_FOLDER = config.data.output_folder

    const REPLACE_PATH = config.data.replace_path

    this.on('contentClassified', async ({ contentCatalog }) => {
        contentCatalog.getFiles().forEach(fileRaw => {
            var file
            if(fileRaw.rel){
                file = fileRaw.rel
            } else {
                file = fileRaw
            }

            if(!file.src.path){
                logger.fatal("This should not happen, the file was not found")
                throw new Error;
            }

            var path = OUTPUT_FOLDER + "/" + file.src.path

            REPLACE_PATH.forEach(element => {
                if(path.substring(0,path.lastIndexOf('/')).includes(element.this)){
                    logger.debug("replacing " + element.this + " with " + element.that)
                    path = path.substring(0, path.indexOf('ROOT')) + element.that + path.substring(path.lastIndexOf('/'))
                }
            });

            const folderPath = path.substring(0,path.lastIndexOf('/'))

            if (!fs.existsSync(folderPath))
            fs.mkdirSync(folderPath, { recursive: true });
            try {
                fs.writeFileSync(path, file._contents);
                logger.debug("successfully wrote: '" + path + "'")
            } catch (error) {
                logger.fatal(error)
                throw new Error;
            }
        });
    })
}
