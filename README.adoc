= Export Content Extension
Jonah Wille <jonahtec2003@gmail.com>

ifdef::embedded[]
{author} - {email}
endif::[]

== Description 

This extension exports the content-catalog into an folder configured in the Playbook or `Cc/` as default.

IMPORTANT: The extensions are run in sequence as they are listed in the playbook (top to bottom). If there is another content-catalog editing extension whose output should also be exported, then this extension must be required below.
 
== Configuration

=== Playbook

Under the `antora` tag add this:

----
- id: Cc_export
      require: "./lib/export-content-extension/lib/index.js"
      enable: false
      data:
        output_folder: './Cc'
        replace_path:
          - this: 'examples/help' 
            that: 'ROOT/pages/example$help'
----

==== replace_path

The output folder for https://docs.antora.org/antora/latest/family-directories/[families] can be changed.

asciidoctor-pdf does not know families, so includes have to be in the `pages` folder, for example:

`+include::example$help/flx-init.txt[]+`

Then the file needs to be at:
----
modules
└───ROOT
    └───pages
        └───examples$help   <= this is a folder
            └───flx-init.txt
----

===== this

The source of a familie (`$` replaced with `/`), e.G. `include::example$help/felixcore.txt[]` => `this: 'examples/help'`

===== that

The output folder of the family starting at `modules/`

=== Enabling

To enable this extension, add the following to the antora command

----
--extension=Cc_export
----
